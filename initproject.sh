#!/bin/bash
echo "Initiating project on VM"
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

while getopts ":n:p:" opt; do
    case "${opt}" in
        n) NAME=${OPTARG}
        ;;
        p) PROJECT=${OPTARG}
        ;;
        \?)
        echo "Usage: -n NAMEVM -p PROJECT"
        echo "NAMEVM can be any string indicating the destiantion name of the VM instance."
        echo "PROJECT is the name of a project on GIT."
        exit 1
        ;;
    esac
done

multipass exec $NAME git clone git@bitbucket.org:richard5/$PROJECT.git /home/ubuntu/projects/$PROJECT
multipass exec $NAME /home/ubuntu/projects/$PROJECT/scripts/init.sh
