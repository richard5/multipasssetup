# What is Multipass setup?

Multipass setup is a small set of bash scripts to help you with setting up multipass based VM's for further use.

Usage:
Define all the apt packages to be installed and follow up commands you want to execute on the new VM in a cloud-init yaml file. I've included my own for a development vm as an example.

Next step is to spin up the VM with the `setupmultipass.sh` command. It takes the following arguments:
- n with the name of the VM, which will be used to identify the VM in following commands.
- c the number of CPU cores, any integer between 1 and 6
- m the amount of memory to allocate for this VM in bytes with K, M or G Suffix.
- d the size of the disk to allocate for this VM in bytes with K, M or G Suffix.

For example: `./setupmultipass.sh -n devvm -c 3 -m 2G -d 5G` will create a VM with the name devvm, 3 CPU cores, 2GB of memory and 5 Gb of virtual disk space.

Please note that some of the later scripts will use the name of the VM to determine if it is a development or production environmen. The checks are:
- starts with dev* for development
- starts with prod* for production machines

The script will create a timeout warning but this does not mean that it failed, it only takes longer to finish the operation within some arbitrary timeout set by multipass. If you want to make sure if the setup is finished you'll need to look in the logfile `/var/log/cloud-init-output.log` inside the VM which you can access via `multipass shell devvm`. The last line will look like this: `Cloud-init v. 21.4-0ubuntu1~20.04.1 finished at...`

When the setup is finished we need to configure the VM with keys and other settings to be able to use git (I use bitbucket.com). I also use keys so I only have to enter the ssh keyphrase only during start of the VM and not every time. This is especially useful when using cronjobs with git pull commands in them.

I've automated this in the script `initmultipass.sh`, It takes the following argument:
- n with the name of the VM (as defined during setupmultipass.sh).

example: `./initmultipass.sh -n devvm`

Next you can add your own projects using the `initproject` script, the project should use the structure as defined in the template `emptyproject`.
The project should be available in a git repository at bitbucket from which it will be downloaded. It takes the following arguments:
- n with the name of the VM.
- p the name of the repository

example: `./initproject.sh -n devvm -p emptyproject`

The init script will clone the repository and then run the crontab.sh script when available to enable all cron jobs. I've made a repository with the [emptyproject](https://bitbucket.org/richard5/emptyproject/) also available. Use this repo as a template for your future projects to get all the benefits for rapid deployment and setup.
