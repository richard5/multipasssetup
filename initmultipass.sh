#!/bin/bash
echo "Setting up Multipass instance"
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

while getopts ":n:" opt; do
    case "${opt}" in
        n) NAME=${OPTARG}
        ;;
        \?)
        echo "Usage: -n NAMEVM"
        echo "NAMEVM can be any string indicating the name of the VM instance."
        exit 1
        ;;
    esac
done

#multipass transfer source YOUR_IMAGE_NAME_DESTINATION:/destination/
multipass transfer ~/.ssh/richard5-Bitbucket $NAME:/home/ubuntu/.ssh/richard5-Bitbucket
multipass transfer ~/.ssh/richard5-Bitbucket.pub $NAME:/home/ubuntu/.ssh/richard5-Bitbucket.pub
multipass exec $NAME chmod og-rw /home/ubuntu/.ssh/richard5-Bitbucket
multipass exec $NAME chmod og-rw /home/ubuntu/.ssh/richard5-Bitbucket.pub
multipass transfer ./sshconfig $NAME:/home/ubuntu/.ssh/config
multipass transfer ./bash_profile $NAME:/home/ubuntu/.bash_profile
multipass exec $NAME mkdir /home/ubuntu/projects
