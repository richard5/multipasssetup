#!/bin/bash
echo "Creating new Multipass instance"
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

while getopts ":n:c:m:d:" opt; do
    case "${opt}" in
        n) NAME=${OPTARG}
        ;;
        m) MEMORY=${OPTARG}
        ;;
        c) CPU=${OPTARG}
        ;;
        d) DISK=${OPTARG}
        ;;
        \?)
        echo "Usage: -n NAMEVM -c NO-OF-CPU -m MEMORY -d DISKSIZE"
        echo "NAMEVM can be any string."
        echo "NO-OF-CPU any number between 1 and 6."
        echo "MEMORY Positive integers, in bytes, or with K, M, G suffix."
        exit 1
        ;;
    esac
done

echo $NAME $CPU $MEMORY $DISK
multipass launch -n $NAME -c $CPU -m $MEMORY -d $DISK --cloud-init devimage.yaml
